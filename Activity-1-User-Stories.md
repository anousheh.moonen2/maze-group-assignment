User Stories 
1. As a player, I want to move in the map (maze) by tapping on my keyboard, so that I can reach the exit and have fun from it. 
2. As a player, I want to have more challenges if I move to a higher level, so that I can feel my character is better as I achieve. 
3. As a player, I want to see the time limitation so that I can achieve before time runs out. 
4. As a player, I want to be able to talk with NPCs so that I can have a good interactive experience in the game. 
5. As a player, I want to be able to see how much health my character has left, so I know if I should play conservatively or attack enemies to further my progress faster. 
6. As a player, I want to be able to visually see what is in my inventory so I don’t forget what items I have picked up.
7. As a player, I want to be able to see where my character is in the maze, so I don’t get lost and forget where I am.
8. As a player, I want to be able to see how many an enemies are in the maze, so I can decide for myself which enemies I should go to first.
9. As a player, I want to be able to customise aspects of my character so that the game feels more personal and I will feel more inclined to progress.
10. As a player, I want to be able to play a tutorial before starting the maze so that I know how to play and how to finish the game without any ambiguity.

Estimated time:
1. Two weeks
2. Three weeks
3. Two weeks
4. Three weeks
5. Two weeks
6. Two weeks
7. Two weeks
8. Three weeks
9. Three weeks
10. Three weeks
