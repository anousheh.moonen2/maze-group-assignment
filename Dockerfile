# syntax=docker/dockerfile:1

FROM eclipse-temurin:17-jdk-jammy

WORKDIR /app

COPY app/build.gradle settings.gradle.kts gradlew ./
COPY gradle ./gradle

COPY . .

RUN ./gradlew build

ENTRYPOINT ["java","-jar","/app/build/libs/COMP2120-Assignment-3-Workshop-07-Group-B.jar"]
