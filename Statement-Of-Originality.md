# COMP2120/6120 Assignment 3 Statement Of Originality

By signing your name and UID each member of the group acknowledges that all the work in this submission is their own work, except where acknowledged below under references. See also [https://comp.anu.edu.au/courses/comp2120/faq/#soo](https://comp.anu.edu.au/courses/comp2120/faq/#soo).

---

- Group Member 1 Name: Jack Hodges
- Group Member 1 UID: u7481361

---

- Group Member 2 Name: Anousheh Moonen
- Group Member 2 UID: u7481690

---

- Group Member 3 Name: Peilin Liu
- Group Member 3 UID: u7518297

---

- Group Member 4 Name: Jiawei Niu
- Group Member 4 UID: u7603590

---

# References (If any)

- [1] [ChatGPT](https://chat.openai.com/) - Used for the generateSquareMaze() method in the SquareMaze class
- [2] [JavaFX Animation and Binding: Simple Countdown Timer](https://asgteach.com/2011/10/javafx-animation-and-binding-simple-countdown-timer-2/) - Used for countdown timer in LevelLayout
