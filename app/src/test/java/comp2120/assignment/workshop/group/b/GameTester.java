package comp2120.assignment.workshop.group.b;

/**
 *
 * Class for testing the game through simulating user input
 *
 * @author Anousheh Moonen
 *
 */

import javafx.scene.input.KeyCode;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static comp2120.assignment.workshop.group.b.KeyInput.*;
import static org.junit.Assert.assertEquals;

public class GameTester {
    private List<KeyCode> keyCodes = new ArrayList<>();
    private Level testLevel0;
    private SquareMaze testMaze;

    private KeyInput[] keyInputs = {
            D, D, D, W, W, W, D, D, D, D, D, D, S, S, S, D, D, D, S, S, S, D, D, D, S, S, S
    };

    /**
     * Adds the series of key inputs required to get from the entrance to the exit of the maze to the keyCodes field
     */
    private void addKeyCodes() {
        for (KeyInput keyInput: keyInputs) {
            keyCodes.add(keyInput.getKeyCode());
        }
    }

    /**
     * Simulates the start of a new game at level 0
     */
    @Before
    public void startGame() {
        testMaze = new SquareMaze();
        testLevel0 = new Level(0, 100, testMaze);
        addKeyCodes();
    }

    /**
     * Simulates player movement to the right and checks that they are positioned at the next cell following this
     * movement
     */
    @Test
    public void testMovement() throws Exception {
        Player testPlayer = new Player(testLevel0);
        testPlayer.handleKeyEvent(KeyCode.D, 3);
        testPlayer.handleKeyEvent(KeyCode.D, 3);
        testPlayer.handleKeyEvent(KeyCode.D, 3);
        testPlayer.handleKeyEvent(KeyCode.D, 3);
        assertEquals(new Point(0, 1), testPlayer.position);
    }

    /**
     * Checks that the player cannot move out of the screen or up and down outside the maze
     */
    @Test
    public void testOutOfBounds() throws Exception {
        Player testPlayer = new Player(testLevel0);
        testPlayer.handleKeyEvent(KeyCode.S, 3);
        testPlayer.handleKeyEvent(KeyCode.S, 3);
        testPlayer.handleKeyEvent(KeyCode.S, 3);
        testPlayer.handleKeyEvent(KeyCode.S, 3);
        assertEquals(new Point(-1, 1), testPlayer.position);
        testPlayer.handleKeyEvent(KeyCode.W, 3);
        testPlayer.handleKeyEvent(KeyCode.W, 3);
        testPlayer.handleKeyEvent(KeyCode.W, 3);
        testPlayer.handleKeyEvent(KeyCode.W, 3);
        assertEquals(new Point(-1, 1), testPlayer.position);
        testPlayer.handleKeyEvent(KeyCode.A, 3);
        testPlayer.handleKeyEvent(KeyCode.A, 3);
        testPlayer.handleKeyEvent(KeyCode.A, 3);
        testPlayer.handleKeyEvent(KeyCode.A, 3);
        assertEquals(new Point(-1, 1), testPlayer.position);
    }

    /**
     * Checks that the player cannot run into a wall
     */
    @Test
    public void testWalls() throws Exception {
        Player testPlayer = new Player(testLevel0);
        testPlayer.position = new Point(testMaze.entrance.x, testMaze.entrance.y);
        testPlayer.handleKeyEvent(KeyCode.D, 3);
        testPlayer.handleKeyEvent(KeyCode.D, 3);
        testPlayer.handleKeyEvent(KeyCode.D, 3);
        testPlayer.handleKeyEvent(KeyCode.D, 3);
        assertEquals(testMaze.entrance, testPlayer.position);
        testPlayer.handleKeyEvent(KeyCode.S, 3);
        testPlayer.handleKeyEvent(KeyCode.S, 3);
        testPlayer.handleKeyEvent(KeyCode.S, 3);
        testPlayer.handleKeyEvent(KeyCode.S, 3);
        assertEquals("expected position to be " + testMaze.entrance.toString() + " because there was a south" +
                "wall so you cannot move down, but it was actually " + testMaze.entrance.toString(),
                testMaze.entrance, testPlayer.position);
    }

    /**
     * Checks that the player ends up at the exit of the maze when they press the series of keys required to get from
     * the entrance to the exit
     */
    @Test
    public void testMazeTraversal() throws Exception {
        Player testPlayer = new Player(testLevel0);

        for (KeyCode keyCode: keyCodes) {
            testPlayer.handleKeyEvent(keyCode, 3);
        }
        assertEquals("expected position to be " + testMaze.exit.toString() + " but was " +
                        testPlayer.position.toString(), testMaze.exit, testPlayer.position);
    }

    /**
     * Checks that the player cannot exit the maze without the key
     */
    @Test
    public void testExitWithNoKey() throws Exception {
        Player testPlayer = new Player(testLevel0);
        testPlayer.position = new Point(testLevel0.maze.exit.x, testLevel0.maze.exit.y);
        testPlayer.handleKeyEvent(KeyCode.D, 3);
        assertEquals("expected player to be unable to exit without the key but actually they moved " +
                testPlayer.movementInCellX, 0, testPlayer.movementInCellX);
    }
}