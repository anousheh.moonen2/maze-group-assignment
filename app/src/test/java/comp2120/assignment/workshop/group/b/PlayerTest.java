package comp2120.assignment.workshop.group.b;

/**
 * @author Jack Hodges
 * @author Jiawei Niu
 */

import org.junit.Test;
import static org.junit.Assert.*;

public class PlayerTest {
    Player testPlayer = new Player(LevelSetup.LEVEL1);

    /**
     * Test case to verify that when the player only have 1 live, after resetting, the player should have 3 lives.
     */
    @Test
    public void testResetLives() {
        testPlayer.lives--;
        testPlayer.lives--;
        assertEquals(1, testPlayer.lives);
        testPlayer.resetLives();
        assertEquals(3, testPlayer.lives);
    }

    /**
     * Test case to verify that when the player get one key in a level, after resetting, the resetKey()
     * method successfully reset the inventory.
     */
    @Test
    public void testResetKey() {
        // test one key
        testPlayer.inventory.add(Item.KEY);
        assertTrue(testPlayer.inventory.contains(Item.KEY));
        testPlayer.resetKey();
        assertFalse(testPlayer.inventory.contains(Item.KEY));
    }

    /**
     * Test case to verify that when the player get multiple keys in a level, after resetting, the resetKey()
     * method successfully reset the inventory.
     */
    @Test
    public void testMultipleKeys() {
        // test two keys -> added remove all to go for this
        testPlayer.inventory.add(Item.KEY);
        testPlayer.inventory.add(Item.KEY);
        assertTrue(testPlayer.inventory.contains(Item.KEY));
        testPlayer.resetKey();
        assertFalse(testPlayer.inventory.contains(Item.KEY));
    }

    /**
     * Test case to verify that when the player do not get a key in a level, after resetting, the resetKey() method
     * successfully reset the inventory.
     */
    @Test
    public void testNoKeys() {
        assertFalse(testPlayer.inventory.contains(Item.KEY));
        testPlayer.resetKey();
        assertFalse(testPlayer.inventory.contains(Item.KEY));
    }

    /**
     * Test case to verify that the resetTime() method successfully reset the timer.
     */
    @Test
    public void testResetTime() {
        testPlayer.timeLeft = 10;
        assertNotEquals(testPlayer.timeLeft, testPlayer.level.timeToComplete);
        testPlayer.resetTime();
        assertEquals(testPlayer.timeLeft, testPlayer.level.timeToComplete);
    }

    /**
     * Test case to verify that the resetPosition() method successfully reset the entrance position.
     */
    @Test
    public void testResetPosition() {
        testPlayer.position.x = 100;
        testPlayer.position.y = 100;
        assertNotEquals(new Point(testPlayer.position.x, testPlayer.position.y), new Point(testPlayer.level.maze.entrance.x - 1, testPlayer.level.maze.entrance.y));
        testPlayer.resetPosition();
        assertEquals(new Point(testPlayer.position.x, testPlayer.position.y), new Point(testPlayer.level.maze.entrance.x - 1, testPlayer.level.maze.entrance.y));
    }

    /**
     * Test case to verify that the addTime() method successfully add the time to the timer.
     */
    @Test
    public void testAddTime() {
        int timeLeft = testPlayer.timeLeft;
        testPlayer.addTime(10);
        assertEquals(testPlayer.timeLeft, timeLeft + 10);
    }

    /**
     * Test case to verify that the loseLife() method reduce the live by one for the player.
     */
    @Test
    public void testLoseLife() {
        int currLives = testPlayer.lives;
        testPlayer.loseLife();
        assertEquals(currLives-1, testPlayer.lives);
    }

    /**
     * Test case to verify that the addKey() method successfully add one key to inventory for the player.
     */
    @Test
    public void testAddKey() {
        assertFalse(testPlayer.inventory.contains(Item.KEY));
        testPlayer.addKey();
        assertTrue(testPlayer.inventory.contains(Item.KEY));
    }

    /**
     * Test case to ensure a player should only exit the level with the key.
     */
    @Test
    public void testEscapeLevel() {
        testPlayer.addKey();
        testPlayer.level = LevelSetup.LEVEL1;
        assertTrue(testPlayer.escapeLevel(testPlayer.level));
        testPlayer.resetKey();
        // Without key
        assertFalse(testPlayer.escapeLevel(testPlayer.level));

        // With key
        testPlayer.addKey();
        assertTrue(testPlayer.escapeLevel(testPlayer.level));
    }

    /**
     * Test case to ensure a player should return to level1 if the player has no lives.
     */
    @Test
    public void restartLevel() {
        testPlayer.restartLevel();
        assertEquals(3, testPlayer.lives);
        assertEquals(LevelSetup.LEVEL1, testPlayer.level);
    }

    /**
     * Test case to verify that the checkLeft method successfully test the cases.
     */
    @Test
    public void testCheckLeft() {
        // below zero check
        testPlayer.position.x = -10;
        assertFalse(testPlayer.checkLeft());
        // out of bounds check
        testPlayer.position.x = testPlayer.level.maze.size;
        assertFalse(testPlayer.checkLeft());
    }
}
