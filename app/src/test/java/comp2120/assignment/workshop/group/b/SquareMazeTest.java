package comp2120.assignment.workshop.group.b;

/**
 * @author Jiawei Niu
 * @author Anousheh Moonen
 */

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class SquareMazeTest {
    Cell testCell = new Cell(new Point(1,1), false, false, true, true);
    SquareMaze mazeInstance = new SquareMaze(10, 5);

    private SquareMaze mazeGenerator;
    private Cell[][] maze;

    @Before
    public void setUp() {
        mazeGenerator = new SquareMaze(10, 3);
        maze = mazeGenerator.grid;
    }

    /**
     * Test case to verify that the 'generateMaze()' method generates a maze of the desired size
     */
    @Test
    public void testMazeSize() {
        // Verify that the generated maze has the correct size
        assertEquals(10, maze.length);
        assertEquals(10, maze[0].length);
    }

    /**
     * Test case to verify that the 'generateMaze()' method generates a maze with the entrance in the first column and
     * the exit in the last column
     */
    @Test
    public void testEntranceAndExit() {
        // Verify that the entrance and exit points are valid
        assertNotNull(mazeGenerator.entrance);
        assertEquals("expected entrance to be in first column but was actually in column " +
                mazeGenerator.entrance.x, 0, mazeGenerator.entrance.x);
        assertNotNull(mazeGenerator.exit);
        assertEquals("expected exit to be in last column (x coordinate 9) but was actually in column " +
                mazeGenerator.exit.x, 9, mazeGenerator.exit.x);
    }

    /**
     * Test case to verify that the 'generateMaze()' method generates a maze with the entrance having no west wall and
     * the exit having no east wall
     */
    @Test
    public void testMazeValidity() {
        assertFalse("expected no westWall at entrance",
                maze[mazeGenerator.entrance.y][mazeGenerator.entrance.x].westWall);
        // Ensure that the entrance has no west wall
        assertFalse("expected no eastWall at exit", maze[mazeGenerator.exit.y][mazeGenerator.exit.x].eastWall);
        // Ensure that the exit has no east wall
    }

    /**
     * Test case to ensure that currentCellRow() method correctly returns the row index (y coordinate) of the testCell
     * object, even if it is modified to a different value, including negative values.
     */
    @Test
    public void testCurrentCellRow() {
        assertEquals(1, mazeInstance.currentCellRow(testCell));
        testCell.position.y = 10;
        assertEquals(10, mazeInstance.currentCellRow(testCell));
        testCell.position.y = -10;
        assertEquals(-10, mazeInstance.currentCellRow(testCell));
    }

    /**
     * Test case to ensure that currentCellCol() method correctly returns the column index (x coordinate) of the
     * testCell object, even if it is modified to a different value, including negative values.
     */
    @Test
    public void currentCellCol() {
        assertEquals(1, mazeInstance.currentCellCol(testCell));
        testCell.position.x = 10;
        assertEquals(10, mazeInstance.currentCellCol(testCell));
        testCell.position.x = -10;
        assertEquals(-10, mazeInstance.currentCellCol(testCell));
    }

    /**
     * Test case to ensure that the 2D grid of Cells can be converted to a 1D list of cells
     */
    @Test
    public void testTo1DList() {

        Cell[][] cells = new Cell[2][2];

        Cell cell1 = new Cell(new Point(0, 0), true, true, false, false);
        Cell cell2 = new Cell(new Point(0, 1), false, true, true, false);
        Cell cell3 = new Cell(new Point(1, 0), true, false, false, true);
        Cell cell4 = new Cell(new Point(1, 1), false, false, true, true);

        cells[0][0] = cell1;
        cells[0][1] = cell2;
        cells[1][0] = cell3;
        cells[1][1] = cell4;

        SquareMaze squareMaze = new SquareMaze(2,1);

        List<Cell> resultList = squareMaze.to1DList(cells);

        assertEquals(4, resultList.size());

        assertTrue(resultList.contains(cell1));
        assertTrue(resultList.contains(cell2));
        assertTrue(resultList.contains(cell3));
        assertTrue(resultList.contains(cell4));
    }
}