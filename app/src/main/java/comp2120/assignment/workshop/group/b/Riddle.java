package comp2120.assignment.workshop.group.b;

/**
 *
 * Used to create riddles with a question and answer that the player faces when they encounter an enemy
 *
 * @author Anousheh Moonen
 */

public class Riddle {
    String question;
    String answer;
    int answerNum;

    public Riddle(String question, String answer, int answerNum) {
        this.question = question;
        this.answer = answer;
        this.answerNum = answerNum;
    }
}
