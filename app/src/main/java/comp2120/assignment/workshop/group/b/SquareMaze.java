package comp2120.assignment.workshop.group.b;

/**
 *
 * A class used to create the square maze with a size, entrance point, exit point and list of enemies with their
 * positions
 * @see ChatGPT was used for the generateSquareMaze() method
 *
 * @author Anousheh Moonen
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SquareMaze {

    // The width and height of this maze
    int size;

    // A 2D list of all the cells in this maze
    Cell[][] grid;

    // A 1D list of all the cells in this maze
    List<Cell> cells;

    // The backend coordinates of this maze entrance
    Point entrance;

    // The backend coordinates of this maze exit
    Point exit;

    // The list of enemies in this maze

    List<Enemy> enemies = new ArrayList<>();

    // The list of backend points occupied by enemies in this maze
    List<Point> enemyPoints = new ArrayList<>();

    // The number of enemies in this maze
    int numberOfEnemies;

    /**
     * @author Anousheh Moonen
     * @param size int of the size of the maze
     * @param numberOfEnemies int representing number of enemeies in the maze
     */
    public SquareMaze(int size, int numberOfEnemies) {
        this.size = size;
        this.grid = generateSquareMaze();
        this.cells = to1DList(grid);
        this.numberOfEnemies = numberOfEnemies;
        addEnemiesToMaze();
    }

    // Empty constructor used to create an instance of the fixed maze used for testing player movement in GameTester
    public SquareMaze() {
        this.grid = createFixedGrid();
        this.cells = createFixedMaze();
        this.size = 5;
        this.entrance = new Point(0, 1);
        this.exit = new Point(4, 3);
        this.numberOfEnemies = 1;
        this.enemies.add(new Enemy(RIDDLES[0], 1, 10, new Point(2, 4)));
        this.enemyPoints.add(new Point(2, 4));
    }

    // Array of riddles used in the game
    public static final Riddle[] RIDDLES = {
            new Riddle("does 1+1 = 1?", "2",2),
            new Riddle("is the sky up?", "the sky",1),
            new Riddle("is a deer with no eyes an idea?", "no idea",2),
            new Riddle("is a deer with no eyes and no legs a still no idea?", "still no idea",1),
            new Riddle("is a fish with no eyes a fishy?", "a fsh",2),
            new Riddle("is a fish with no eyes and no f's and no s's an h", "a h",1)
    };


    /**
     * @author Anousheh Moonen
     * @return a random maze of the size in this squareMaze specified in the SquareMaze constructor
     */
    public Cell[][] generateSquareMaze() {
        Cell[][] maze = new Cell[size][size];

        // Initialize the maze with cells
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                maze[i][j] = new Cell(new Point(j, i), true, true, true, true);
            }
        }

        // Create a random number generator
        Random random = new Random();

        // Entrance: Left wall (column 0)
        int entranceRow = random.nextInt(size); // Entrance can be on any row
        maze[entranceRow][0].westWall = false; // Disable the left wall of the entrance cell
        maze[entranceRow][0].visited = true; // Mark the entrance cell as visited

        // Exit: Right wall (column size - 1)
        int exitRow;
        do {
            exitRow = random.nextInt(size); // Exit can be on any row
        } while (entranceRow == exitRow);

        // Add walls all around the outside
        for (int i = 0; i < size; i++) {
            maze[i][0].westWall = true; // Left wall
            maze[i][size - 1].eastWall = true; // Right wall
            maze[0][i].northWall = true;
            maze[size - 1][i].southWall = true;
        }

        // Initialize a list of cells to visit
        List<Cell> stack = new ArrayList<>();
        Cell currentCell = maze[entranceRow][0]; // Entrance is on the left wall
        currentCell.visited = true;

        // Recursive backtracking algorithm
        while (true) {
            List<Cell> neighbors = new ArrayList<>();

            int row = currentCellRow(currentCell);
            int col = currentCellCol(currentCell);

            // Check north neighbor
            if (row > 0 && !maze[row - 1][col].visited) {
                neighbors.add(maze[row - 1][col]);
            }
            // Check south neighbor
            if (row < size - 1 && !maze[row + 1][col].visited) {
                neighbors.add(maze[row + 1][col]);
            }
            // Check east neighbor
            if (col < size - 1 && !maze[row][col + 1].visited) {
                neighbors.add(maze[row][col + 1]);
            }
            // Check west neighbor
            if (col > 0 && !maze[row][col - 1].visited) {
                neighbors.add(maze[row][col - 1]);
            }

            if (!neighbors.isEmpty()) {
                Cell randomNeighbor = neighbors.get(random.nextInt(neighbors.size()));
                int newRow = currentCellRow(randomNeighbor);
                int newCol = currentCellCol(randomNeighbor);

                // Remove wall between current cell and the chosen neighbor
                if (newRow < row) {
                    currentCell.northWall = false;
                    maze[newRow][newCol].southWall = false;
                } else if (newRow > row) {
                    currentCell.southWall = false;
                    maze[newRow][newCol].northWall = false;
                } else if (newCol < col) {
                    currentCell.westWall = false;
                    maze[newRow][newCol].eastWall = false;
                } else if (newCol > col) {
                    currentCell.eastWall = false;
                    maze[newRow][newCol].westWall = false;
                }

                stack.add(currentCell);
                currentCell = maze[newRow][newCol];
                currentCell.visited = true;
            } else if (!stack.isEmpty()) {
                currentCell = stack.remove(stack.size() - 1);
            } else {
                break;
            }
        }

        // Set the exit on the right wall
        maze[exitRow][size - 1].eastWall = false;
        maze[entranceRow][0].westWall = false; // Disable the left wall of the entrance cell
        maze[entranceRow][0].visited = true; // Mark the entrance cell as visited

        this.entrance = new Point(0, entranceRow);
        this.exit = new Point(size - 1, exitRow);

        return maze;
    }

    // Adds enemies randomly to the maze
    private void addEnemiesToMaze() {
        Random random = new Random();
        for (int i = 0; i < numberOfEnemies; i++) {
            enemies.add(new Enemy(RIDDLES[random.nextInt(RIDDLES.length)],
                    0, 10, new Point(random.nextInt(size), random.nextInt(size))));
            enemyPoints.add(enemies.get(i).position);
        }
        // Randomly select one of the enemies to have the key
        enemies.get(random.nextInt(numberOfEnemies)).key = 1;
    }


    // Used in the generateSquareMaze() method
    public int currentCellRow(Cell cell) {
        return cell.position.y;
    }

    // Used in the generateSquareMaze() method
    public int currentCellCol(Cell cell) {
        return cell.position.x;
    }

    /**
     * @author Anousheh Moonen
     * @param cells an 2D array of cells that represent status of walls in the maze
     * @return a list of cells in a single List<Cell>
     */
    public List<Cell> to1DList(Cell[][] cells) {
        List<Cell> result = new ArrayList<>();
        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells.length; j++) {
                result.add(cells[i][j]);
            }
        }
        return result;
    }

    /**
     * @author Anousheh Moonen
     * @return a 2D array of cells specifying a fixed maze used in the GameTester class
     */
    public Cell[][] createFixedGrid() {
        Cell[][] result = new Cell[5][5];
        result[0][0] = new Cell(new Point(0,0), true, true, false, false);
        result[0][1] = new Cell(new Point(1, 0), false, true, false, true);
        result[0][2] = new Cell(new Point(2,0), false, true, true, false);
        result[0][3] = new Cell(new Point(3,0), true, true, false, true);
        result[0][4] = new Cell(new Point(4, 0), false, true, true, false);
        result[1][0] = new Cell(new Point(0, 1), false, false, true, true);
        result[1][1] = new Cell(new Point(1, 1), true, true, true, false);
        result[1][2] = new Cell(new Point(2, 1), true, false, false, true);
        result[1][3] = new Cell(new Point(3, 1), true, true, true, false);
        result[1][4] = new Cell(new Point(4, 1), true, false, true, false);
        result[2][0] = new Cell(new Point(0, 2), true, true, false, true);
        result[2][1] = new Cell(new Point(1, 2), false, false, false, true);
        result[2][2] = new Cell(new Point(2, 2), false, true, true, false);
        result[2][3] = new Cell(new Point(3, 2), true, false, false, true);
        result[2][4] = new Cell(new Point(4, 2), false, false, true, false);
        result[3][0] = new Cell(new Point(0, 3), true, true, false, false);
        result[3][1] = new Cell(new Point(1, 3), false, true, false, true);
        result[3][2] = new Cell(new Point(2, 3), false, false, true, true);
        result[3][3] = new Cell(new Point(3, 3), true, true, false, false);
        result[3][4] = new Cell(new Point(4, 3), false, false, false, true);
        result[4][0] = new Cell(new Point(0, 4), true, false, false, true);
        result[4][1] = new Cell(new Point(1, 4), false, true, false, true);
        result[4][2] = new Cell(new Point(2, 4), false, true, false, true);
        result[4][3] = new Cell(new Point(3, 4), false, false, false, true);
        result[4][4] = new Cell(new Point(4, 4), false, true, true, true);
        return result;
    }

    /**
     * @author Anousheh Moonen
     * @return a list of cells in a single List<Cell> with the cells in the fixed maze used in the GameTester class
     */
    public List<Cell> createFixedMaze() {
        return to1DList(createFixedGrid());
    }
}