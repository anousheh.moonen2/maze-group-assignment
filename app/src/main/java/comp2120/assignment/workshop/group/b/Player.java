/**
 *
 * The backend Player object playing the game
 *
 * @author Anousheh Moonen
 */
package comp2120.assignment.workshop.group.b;

/**
 * @author Jiawei Niu
 * @author Anousheh Moonen
 */

import javafx.scene.input.KeyCode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Player {

    // Number of lives the player has (from 0 to 3 inclusive)
    public int lives = 3;

    // A list of inventory items
    public List<Item> inventory = new ArrayList<Item>();

    // The current level the player is on (we start the player at the tutorial the very first time they play the game)
    public Level level = LevelSetup.LEVEL0;

    // Time the player has left
    public int timeLeft = level.timeToComplete;

    // The position of the player
    public Point position;

    // The movement in the cell of the player (in the frontend, the increment by which the player moves is probably
    // less than the width of the cell, so we need to keep track of how far the player has moved in their cell)

    public int movementInCellX;
    public int movementInCellY;
    public boolean readyToExit = false;

    public Player(Level level) {
        this.level = level;
        this.position = new Point(level.maze.entrance.x - 1, level.maze.entrance.y);
    }

    public void resetLives() {
        this.lives = 3;
    }

    public void resetKey() {
        // changed code to remove all instances of keys, if for some reason there is more than one key in it
        this.inventory.removeAll(Collections.singleton(Item.KEY));
    }

    public void resetTime() {
        this.timeLeft = level.timeToComplete;
    }

    public void resetLevel() {
        if (! (this.level == LevelSetup.LEVEL1)) {
            for (int i = 1; i < LevelSetup.levels.length; i++) {
                if (this.level == LevelSetup.levels[i]) {
                    this.level = LevelSetup.levels[i-1];
                    break;
                }
            }
        }
    }

    public void resetPosition() {
        this.position = new Point(this.level.maze.entrance.x - 1, this.level.maze.entrance.y);
    }

    public void addTime(float time) {
        this.timeLeft += time;
    }

    // If the player has no lives left, they must restart from level 1. Else, they restart the current level
    public void loseLife() {
        this.lives -= 1;
        if (this.lives == 0) {
            restartLevel();
        }
        else {
            resetPosition();
            resetTime();
            resetKey();
        }
    }

    public void addKey() {
        this.inventory.add(Item.KEY);
    }

    // Only run this method when the player is at the exit (i.e. position of player = position of exit)
    /**
     * @param level level from which the player is escaping
     * @return true if the player can escape, false if not
     */
    public boolean escapeLevel(Level level) {
        if (inventory.contains(Item.KEY)) {
            if (this.level.levelNumber != 3) {
                this.level = LevelSetup.levels[level.levelNumber + 1];
                // Remove the key from the inventory so that the player doesn't have the key in the next level
                resetKey();
                resetPosition();
                resetTime();
            }
            return true;
        }
        return false;
    }

    // Run this method when the player has no lives left or they run out of time
    public void restartLevel() {
        resetLives();
        resetLevel();
        resetPosition();
        resetKey();
        resetTime();
    }

    /**
     * @return true if there is no west wall
     */
    public boolean checkLeft() {
        if (position.x < 0) {
            return false;
        }
        if (position.x == this.level.maze.size) {
            return false;
        }
        if (this.level.maze.grid[position.y][position.x].westWall) {
            return false;
        }
        return true;
    }

    /**
     * @return true if there is no east wall
     */
    public boolean checkRight() {
        if (this.position.equals(this.level.maze.exit) && movementInCellY == 0) {
            readyToExit = true;
            return false;
        }
        if (readyToExit) {
            readyToExit = false;
        }
        if (position.x < 0) {
            return true;
        }
        if (position.x == this.level.maze.size) {
            return false;
        }
        if (this.level.maze.grid[position.y][position.x].eastWall) {
            return false;
        }
        return true;
    }

    /**
     * @return true if there is no north wall
     */
    public boolean checkUp() {
        if (position.x < 0) {
            return false;
        }
        if (this.level.maze.grid[position.y][position.x].northWall) {
            return false;
        }
        return true;
    }

    /**
     * @return true if there is no south wall
     */
    public boolean checkDown() {
        if (this.position.x < 0) {
            return false;
        }
        if (this.level.maze.grid[position.y][position.x].southWall) {
            return false;
        }
        return true;
    }

    // Updates the position and movement in cell of the player if the movement is allowed
    public void handleKeyEvent(KeyCode code, int ratio) throws Exception {
        if (code == KeyCode.W || code == KeyCode.UP) {
            if (movementInCellX == 0) {
                if (movementInCellY == 0 && checkUp()) {
                    movementInCellY = ratio - 1;
                    this.position.y -= 1;
                } else if (movementInCellY > 0) {
                    movementInCellY -= 1;
                    if (movementInCellY == 0) {
                        if (this.level.maze.enemyPoints.contains(this.position)) {
                            for(Enemy enemy:this.level.maze.enemies ){
                                if (enemy.position.equals(this.position)){
                                    LevelLayout.encounterEnemy(enemy);
                                }
                            }
                        }
                    }
                }
            }
        }
        if (code == KeyCode.S || code == KeyCode.DOWN) {
            if (movementInCellX == 0) {
                if (checkDown()) {
                    if (movementInCellY == 0) {
                        movementInCellY += 1;
                    } else if (movementInCellY > 0 && movementInCellY < ratio - 1) {
                        movementInCellY += 1;
                    } else if (movementInCellY == ratio - 1) {
                        movementInCellY = 0;
                        this.position.y += 1;
                        if (this.level.maze.enemyPoints.contains(this.position)) {
                            for(Enemy enemy:this.level.maze.enemies ){
                                if (enemy.position.equals(this.position)){
                                    LevelLayout.encounterEnemy(enemy);
                                }
                            }
                        }
                    }
                }
            }
        }
        if (code == KeyCode.A || code == KeyCode.LEFT) {
            if (movementInCellY == 0) {
                if (movementInCellX == 0 && checkLeft()) {
                    movementInCellX = ratio - 1;
                    this.position.x -= 1;
                } else if (movementInCellX > 0) {
                    movementInCellX -= 1;
                    if (movementInCellX == 0) {
                        if (this.level.maze.enemyPoints.contains(this.position)) {
                            for(Enemy enemy:this.level.maze.enemies ){
                                if (enemy.position.equals(this.position)){
                                    LevelLayout.encounterEnemy(enemy);
                                }
                            }
                        }
                    }
                }
            }
        }
        if (code == KeyCode.D || code == KeyCode.RIGHT) {
            if (movementInCellY == 0) {
                if (checkRight()) {
                    if (movementInCellX == 0) {
                        movementInCellX += 1;
                    } else if (movementInCellX > 0 && movementInCellX < ratio - 1) {
                        movementInCellX += 1;
                    } else if (movementInCellX == ratio - 1) {
                        this.position.x += 1;
                        movementInCellX = 0;
                        if (this.level.maze.enemyPoints.contains(this.position)) {
                            for(Enemy enemy:this.level.maze.enemies ){
                                if (enemy.position.equals(this.position)){
                                    LevelLayout.encounterEnemy(enemy);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}