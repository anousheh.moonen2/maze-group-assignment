package comp2120.assignment.workshop.group.b;

import javafx.animation.*;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * @author Peilin Liu
 */

public class FailAnswerPage extends Application {
    private double xPosition = 0; // defult position of player
    private double yPosition = 300; // defult position

    Image[] mans = {
            new Image("/Assets/RunningMan1.png"),
            new Image("/Assets/RunningMan2.png")
    };

    private int currentImageIndex = 0;

    private TranslateTransition transitionX;
    private TranslateTransition transitionY;
    private Player player;

    // Note that because there is no default empty constructor, you cannot start from the FailAnswerPage

    public FailAnswerPage(Player player) {
        System.out.println("time left is " + player.timeLeft);
        System.out.println("number of lives left is " + player.lives);
        this.player = player;
    }

    @Override
    public void start(Stage primaryStage) {
        // create Canvas(HD)
        Canvas canvas = new Canvas(1200, 1000);

        Text tutorialText = new Text("You failed to escape");

        // stack plane to demonstrate eth
        StackPane root = new StackPane();
        root.setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));
        root.setAlignment(Pos.CENTER);
        // set scene
        Scene scene = new Scene(root, 1200, 1000);
        // create buttons
        Button exitButton = createButton("Main menu");
        Button startButton = createButton("Let me try again.");
        HBox buttonsHBox = new HBox(10);
        buttonsHBox.setAlignment(Pos.CENTER);
        buttonsHBox.getChildren().addAll(startButton, exitButton);
        buttonsHBox.setTranslateY(100);

        //jump to main menu
        exitButton.setOnAction(event -> {
            GameMenu gameMenu = new GameMenu(player);
            try {
                gameMenu.start(new Stage());
                ((Stage) exitButton.getScene().getWindow()).close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        //jump to game
        startButton.setOnAction(event -> {
            Stage stage = (Stage) startButton.getScene().getWindow();
            stage.close();
            LevelLayout levelLayout = new LevelLayout(player);
            try {
                levelLayout.start(new Stage());
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        });

        ImageView man1 = new ImageView(mans[0]);
        ImageView man = new ImageView(mans[currentImageIndex]);
        man.setTranslateX(xPosition);
        man.setTranslateY(yPosition);

        root.getChildren().add(man);

        transitionX = new TranslateTransition(Duration.millis(100), man);
        transitionY = new TranslateTransition(Duration.millis(100), man);

        // make character move with wasd
        scene.setOnKeyPressed(event -> {
            KeyCode code = event.getCode();
            if (code == KeyCode.W) {
                yPosition -= 10;
            } else if (code == KeyCode.S) {
                yPosition += 10;
            } else if (code == KeyCode.A) {
                xPosition -= 10;
            } else if (code == KeyCode.D) {
                xPosition += 10;
            }
            currentImageIndex = (currentImageIndex + 1) % mans.length;
            man.setImage(mans[currentImageIndex]);
            man.setTranslateX(xPosition);
            man.setTranslateY(yPosition);
        });

        man1.fitWidthProperty().bind(scene.widthProperty());
        man1.fitHeightProperty().bind(scene.heightProperty());
        man1.setPreserveRatio(true);

        tutorialText.setFont(Font.font("Arial", FontWeight.BOLD, 40));
        tutorialText.setFill(Color.WHITE);
        tutorialText.setTranslateX(0);
        tutorialText.setTranslateY(-300);
        root.getChildren().add(tutorialText);
        root.getChildren().add(buttonsHBox);

        primaryStage.setScene(scene);
        primaryStage.setTitle("Game start");
        primaryStage.show();
    }

    // button style

    /**
     * @author Peilin Liu
     * @param text takes in the text for a button
     * @return the button with the text
     */
    private Button createButton(String text) {
        Button button = new Button(text);
        button.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        button.setStyle("-fx-background-color: #333333; -fx-text-fill: #ffffff;");
        return button;
    }

    public int getLives() {
        return player.lives;
    }

    public void setLives(int lives) {
        player.lives = lives;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
