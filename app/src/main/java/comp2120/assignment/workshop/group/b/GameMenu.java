package comp2120.assignment.workshop.group.b;

/**
 * @author Peilin Liu
 */

import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;

public class GameMenu extends Application {

    private double xPosition = 0; // default position of player
    private double yPosition = 300; // default position

    Image[] mans = {
            new Image("/Assets/RunningMan1.png"),
            new Image("/Assets/RunningMan2.png")
    };

    private int currentImageIndex = 0;

    private TranslateTransition transitionX;
    private TranslateTransition transitionY;
    Player player;

    public GameMenu(Player player) {
        this.player = player;
    }
    
    // Default empty constructor which is used to initialise the player when the game is first launched

    public GameMenu() {
        this.player = new Player(LevelSetup.LEVEL0);
    }


    @Override
    public void start(Stage primaryStage) {

        // create Canvas(HD)
        Canvas canvas = new Canvas(1200, 1000);

        // get GraphicsContext
        GraphicsContext gc = canvas.getGraphicsContext2D();

        // light view
        gc.setFill(Color.rgb(255, 255, 153)); // light like
        //bond with scene
        canvas.widthProperty().bind(primaryStage.widthProperty());
        canvas.heightProperty().bind(primaryStage.heightProperty());
//      gc.fillPolygon(xPoints, yPoints, 3);
        ChangeListener<Number> resizeListener = new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                double width = primaryStage.getWidth();
                double height = primaryStage.getHeight();

                double[] xPoints = {350, width / 2, width - 350};
                double[] yPoints = {height, 0, height};

                gc.clearRect(0, 0, width, height); // clear paint before
                gc.fillPolygon(xPoints, yPoints, 3);
            }
        };

        // create buttons
        Button startButton = createButton("Start");
        Button tutorialButton = createButton("Tutorial");
        Button exitButton = createButton("Exit");
        Button insButton = createButton("Guide");
        VBox buttonsVBox = new VBox(10);
        buttonsVBox.setAlignment(Pos.CENTER);
        buttonsVBox.getChildren().addAll(startButton, tutorialButton, exitButton, insButton);
        buttonsVBox.setTranslateY(100);

        //set exit action
        exitButton.setOnAction(event -> {
            primaryStage.close();
        });

        // stack plane to demonstrate eth
        StackPane root = new StackPane();
        root.setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));
        root.setAlignment(Pos.CENTER);

        //set tutorial action
        tutorialButton.setOnAction(event -> {
            Tutorial tutorial = new Tutorial();
            try {
                tutorial.start(new Stage());
                ((Stage) tutorialButton.getScene().getWindow()).close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        //jump to start
        startButton.setOnAction(event -> {
            JumpPage jumpPage = new JumpPage(player);
            try {
                jumpPage.start(new Stage());
                ((Stage) startButton.getScene().getWindow()).close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        //jump to instruction
        insButton.setOnAction(event -> {
            InstructionPage instructionPage = new InstructionPage();
            try {
                instructionPage.start(new Stage());
                ((Stage) insButton.getScene().getWindow()).close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        // set buttons position
        startButton.setTranslateY(-50);
        tutorialButton.setTranslateY(0);
        exitButton.setTranslateY(50);
        insButton.setTranslateY(100);

        Image key = new Image("/Assets/Key.png");
        ImageView man1 = new ImageView(mans[0]);
        ImageView key1 = new ImageView(key);
        man1.setTranslateZ(-50);
        man1.setTranslateY(250);
        key1.setTranslateZ(-50);
        key1.setTranslateY(-250);
        // Set size of player and key
        double desiredWidth = 200;
        double desiredHeight = 200;
        double scaleX = desiredWidth / man1.getBoundsInLocal().getWidth();
        double scaleY = desiredHeight / man1.getBoundsInLocal().getHeight();
        man1.setScaleX(scaleX);
        man1.setScaleY(scaleY);
        key1.setScaleX(scaleX/4);
        key1.setScaleY(scaleY/4);
        key1.setRotate(90);

        primaryStage.widthProperty().addListener(resizeListener);
        primaryStage.heightProperty().addListener(resizeListener);
        root.getChildren().add(canvas);
        // set scene
        Scene scene = new Scene(root, 1200, 1000);

        ImageView man = new ImageView(mans[currentImageIndex]);
        man.setTranslateX(xPosition);
        man.setTranslateY(yPosition);

        root.getChildren().add(man);

        transitionX = new TranslateTransition(Duration.millis(100), man);
        transitionY = new TranslateTransition(Duration.millis(100), man);

        // make character move with wasd
        scene.setOnKeyPressed(event -> {
            KeyCode code = event.getCode();
            if (code == KeyCode.W) {
                yPosition -= 10;
            } else if (code == KeyCode.S) {
                yPosition += 10;
            } else if (code == KeyCode.A) {
                xPosition -= 10;
            } else if (code == KeyCode.D) {
                xPosition += 10;
            }
            currentImageIndex = (currentImageIndex + 1) % mans.length;
            man.setImage(mans[currentImageIndex]);
            man.setTranslateX(xPosition);
            man.setTranslateY(yPosition);
        });

        man1.fitWidthProperty().bind(scene.widthProperty());
        man1.fitHeightProperty().bind(scene.heightProperty());
        man1.setPreserveRatio(true);
        key1.fitWidthProperty().bind(scene.widthProperty());
        key1.fitHeightProperty().bind(scene.heightProperty());
        key1.setPreserveRatio(true);

        startButton.prefWidthProperty().bind(scene.widthProperty().multiply(0.1));
        startButton.prefHeightProperty().bind(scene.heightProperty().multiply(0.1));
        tutorialButton.prefWidthProperty().bind(scene.widthProperty().multiply(0.1));
        tutorialButton.prefHeightProperty().bind(scene.heightProperty().multiply(0.1));
        exitButton.prefWidthProperty().bind(scene.widthProperty().multiply(0.1));
        exitButton.prefHeightProperty().bind(scene.heightProperty().multiply(0.1));
        insButton.prefWidthProperty().bind(scene.widthProperty().multiply(0.1));
        insButton.prefHeightProperty().bind(scene.heightProperty().multiply(0.1));

        root.getChildren().addAll(key1);
        root.getChildren().add(buttonsVBox);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Game Menu");
        primaryStage.show();
    }

    /**
     * @author Peilin Liu
     * @param text text for the button to display
     * @return a button with the text on it
     */
    private Button createButton(String text) {
        Button button = new Button(text);
        button.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        button.setStyle("-fx-background-color: #333333; -fx-text-fill: #ffffff;");
        return button;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
