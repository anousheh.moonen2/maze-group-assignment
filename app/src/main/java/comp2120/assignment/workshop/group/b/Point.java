/**
 *
 * The Point class can be used to create points for the backend (so they must be integers, because we represent
 * positions in the backend by a Cartesian grid with unitary increments)
 *
 * @author Anousheh Moonen
 * @author Jack Hodges
 */
package comp2120.assignment.workshop.group.b;

import java.util.Objects;

public class Point {
    public int x;
    public int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * @author Jack Hodges
     *
     * @param o an instance of an object that we want to compare to our point
     * @return true if the Point's contain the same x and y coordinates
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x && y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    public String toString() {
        return "(" + this.x + ", " + this.y + ")";
    }
}

/**
 *
 * The FrontEndPoint class can be used to create points for the frontend (so they are doubles, because these will be
 * used to actually set the position of the player that the user sees on the screen)
 *
 * @author Anousheh Moonen
 */

class FrontEndPoint {
    public double x;
    public double y;

    public FrontEndPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
