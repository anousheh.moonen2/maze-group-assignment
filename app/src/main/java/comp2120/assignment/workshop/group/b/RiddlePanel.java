package comp2120.assignment.workshop.group.b;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.concurrent.atomic.AtomicBoolean;

import static comp2120.assignment.workshop.group.b.LevelLayout.*;

/**
 * @author Peilin Liu
 */

public class RiddlePanel{

    public Riddle riddle;

    public StackPane root;

    public  Enemy enemy;

    public Player player;

    public RiddlePanel(Riddle riddle, StackPane root, Enemy enemy, Player player) {
        System.out.println(enemy.key);
        this.riddle = riddle;
        this.root = root;
        this.enemy = enemy;
        this.player = player;
    }

    public boolean startRiddle(Riddle riddle, StackPane root) throws Exception {
        root.setVisible(true);
        final boolean[] answer = {true};
        System.out.println("I am starting riddle");
        Rectangle puzzleRect = new Rectangle(400, 200, Color.rgb(111, 25, 25, 0.73));
        Label label = new Label(riddle.question);
        label.setFont(Font.font("Arial", FontWeight.BOLD, 30));
        label.setTextFill(Color.WHITE);

        VBox popupContent = new VBox(label);
        popupContent.setAlignment(Pos.CENTER);

        StackPane.setAlignment(popupContent, Pos.CENTER);

        StackPane popupPane = new StackPane(puzzleRect, popupContent);
        popupPane.setAlignment(Pos.CENTER);

        root.getChildren().add(popupPane);

        Button yesButton = createButton("Yes");
        Button noButton = createButton("No");

        yesButton.setStyle("-fx-background-color: #333333; -fx-text-fill: #ffffff;");
        noButton.setStyle("-fx-background-color: #333333; -fx-text-fill: #ffffff;");

        Button continueButton = createButton("Finish");
        continueButton.setStyle("-fx-background-color: #333333; -fx-text-fill: #ffffff;");

        if(riddle.answerNum == 1) {
            System.out.println("check answer num = 1");
            yesButton.setOnAction(event -> {
                popupContent.setAlignment(Pos.CENTER);
                Label successLabel = new Label("You got that riddle right!");
                player1.addKey();
                HBox successBox = new HBox(10, successLabel, continueButton);
                successLabel.setFont(Font.font("Arial", FontWeight.BOLD, 30));
                successLabel.setTextFill(Color.WHITE);
                successLabel.setStyle("-fx-background-color: rgba(111, 25, 25, 0.73);");
                successLabel.setAlignment(Pos.CENTER);

                successBox.setAlignment(Pos.CENTER);
                popupContent.getChildren().add(successBox);
                continueButton.setOnAction(event2 -> {
                    root.setVisible(false);
                    popupPane.getChildren().clear();
                });
                answer[0] = true;
            });
            noButton.setOnAction(event -> {
                System.out.println("check answer num = 0");
                popupContent.setAlignment(Pos.CENTER);
                Label failLabel = new Label("That's not right, back to the start.");
                HBox failBox = new HBox(10, failLabel, continueButton);
                failLabel.setFont(Font.font("Arial", FontWeight.BOLD, 30));
                failLabel.setTextFill(Color.WHITE);
                failLabel.setStyle("-fx-background-color: rgba(111, 25, 25, 0.73);");
                failLabel.setAlignment(Pos.CENTER);
                failBox.setAlignment(Pos.CENTER);
                popupContent.getChildren().add(failBox);

                continueButton.setOnAction(event2 -> {
                    root.setVisible(false);
                    popupPane.getChildren().clear();
                });
                answer[0] = false;

                if(answer[0] == false){
                    player1.resetTime();
                    // Countdown finished, stop the timer
                    timerLabel.setTextFill(Color.DARKRED);
                    timeline.stop();
                    /**
                     * @author Peilin Liu
                     */
                    Stage stage = (Stage) timerLabel.getScene().getWindow();
                    stage.close();
                    if(player1.lives > 1) {
                        player1.lives -= 1;
                        timeline.stop();
                        FailAnswerPage failAnswerPage = new FailAnswerPage(player1);
                        try {
                            failAnswerPage.start(new Stage());
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    } else if(player1.lives == 1) {
                        player1.resetLives();
                        timeline.stop();
                        FailPage failPage = new FailPage(player1);
                        try {
                            failPage.start(new Stage());
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }

                }
            });
            HBox buttonBox = new HBox(10, yesButton, noButton);
            buttonBox.setAlignment(Pos.CENTER);
            popupContent.getChildren().add(buttonBox);
            return answer[0];
        }else{
            System.out.println("check answer num = 2");
            noButton.setOnAction(event -> {
                popupContent.setAlignment(Pos.CENTER);
                Label successLabel = new Label("You got that riddle right!");
                player1.addKey();
                HBox successBox = new HBox(10, successLabel, continueButton);
                successLabel.setFont(Font.font("Arial", FontWeight.BOLD, 30));
                successLabel.setTextFill(Color.WHITE);
                successLabel.setStyle("-fx-background-color: rgba(111, 25, 25, 0.73);");
                successLabel.setAlignment(Pos.CENTER);

                successBox.setAlignment(Pos.CENTER);
                popupContent.getChildren().add(successBox);
                continueButton.setOnAction(event2 -> {
                    root.setVisible(false);
                    popupPane.getChildren().clear();
                });
                answer[0] = true;
            });
            yesButton.setOnAction(event -> {
                popupContent.setAlignment(Pos.CENTER);
                Label failLabel = new Label("That isn't quite right.");
                HBox failBox = new HBox(10, failLabel, continueButton);
                failLabel.setFont(Font.font("Arial", FontWeight.BOLD, 30));
                failLabel.setTextFill(Color.WHITE);
                failLabel.setStyle("-fx-background-color: rgba(111, 25, 25, 0.73);");
                failLabel.setAlignment(Pos.CENTER);
                failBox.setAlignment(Pos.CENTER);
                popupContent.getChildren().add(failBox);

                continueButton.setOnAction(event2 -> {
                    root.setVisible(false);
                    popupPane.getChildren().clear();
                });

                answer[0] = false;

                if(answer[0] == false){
                    player1.resetTime();
                    // Countdown finished, stop the timer
                    timerLabel.setTextFill(Color.DARKRED);
                    timeline.stop();
                    /**
                     * @author Peilin Liu
                     */
                    Stage stage = (Stage) timerLabel.getScene().getWindow();
                    stage.close();
                    if(player1.lives > 1) {
                        player1.lives -= 1;
                        timeline.stop();
                        FailAnswerPage failAnswerPage = new FailAnswerPage(player1);
                        try {
                            failAnswerPage.start(new Stage());
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    } else if(player1.lives == 1) {
                        player1.resetLives();
                        timeline.stop();
                        FailPage failPage = new FailPage(player1);
                        try {
                            failPage.start(new Stage());
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }

                }
            });
            HBox buttonBox = new HBox(10, yesButton, noButton);
            buttonBox.setAlignment(Pos.CENTER);
            popupContent.getChildren().add(buttonBox);
            return answer[0];
        }

    }

    private Button createButton(String text) {
        Button button = new Button(text);
        button.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        button.setStyle("-fx-background-color: #333333; -fx-text-fill: #ffffff;");
        return button;
    }

    private Rectangle createEnemy(){
        return new Rectangle(50, 50, Color.RED);
    }
}
