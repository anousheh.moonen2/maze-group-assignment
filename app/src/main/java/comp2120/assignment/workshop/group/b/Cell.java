package comp2120.assignment.workshop.group.b;

/**
 *
 * The maze grid for each level is created out of these cells
 *
 * @author Anousheh Moonen
 */

public class Cell {
    public Point position;
    public boolean westWall;
    public boolean eastWall;
    public boolean northWall;
    public boolean southWall;
    boolean visited;

    /**
     *
     * @param position the current position of the cell we are studying
     * @param westWall boolean for if it is a west wall
     * @param northWall boolean for if it is a north wall
     * @param eastWall boolean for if it is an east wall
     * @param southWall boolean for if it is a south wall
     */
    public Cell(Point position, boolean westWall, boolean northWall, boolean eastWall, boolean southWall) {
        this.position = position;
        this.visited = false;
        this.westWall = westWall;
        this.northWall = northWall;
        this.eastWall = eastWall;
        this.southWall = southWall;
    }
}
