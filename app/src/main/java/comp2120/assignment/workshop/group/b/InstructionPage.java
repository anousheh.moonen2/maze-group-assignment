package comp2120.assignment.workshop.group.b;

/**
 * @author Peilin Liu
 *
 */

import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

public class InstructionPage extends Application {

    private double xPosition = 0; // defult position of player
    private double yPosition = 300; // defult position
    private double prevXPosition = 0; // defult position of player
    private double prevYPosition = 300; // defult position

    Image[] mans = {
            new Image("/Assets/RunningMan1.png"),
            new Image("/Assets/RunningMan2.png")
    };

    private int currentImageIndex = 0;
    private TranslateTransition transitionX;
    private TranslateTransition transitionY;

    @Override
    public void start(Stage primaryStage) throws Exception {
        StackPane root = new StackPane();
        // set scene
        Scene scene = new Scene(root, 1200, 1000);
        root.setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));
        ImageView newMan = createMan();
        root.getChildren().add(newMan); // keep the character
        newMan.requestFocus();
        // set instruction text
        Text tutorialText = new Text("The game has 4 levels (level 0, level 1, level 2 and level 3). The complexity " +
                "of the maze increases with every level, as do the number of enemies (the red characters in the maze)" +
                " and the time pressure.\n" +

                "Each level has a time limit. The amount of time left for that level is given by the countdown timer at " +
                "the top of the screen.\n" +

                "You will begin at the entrance (you are the running man!) and the aim is to get to the exit of the maze " +
                "(you will know where the exit is because there will be a coloured door instead of the normal white " +
                "wall) before the time for that level runs out.\n" +

                "Move your running man player with either the w a s d keys on your keyboard " + "(directions are " +
                "self-explanatory). Obviously, you cannot pass through walls.\n" +

                "When you run into an enemy, you must answer a riddle. Remember, every precious second you waste " +
                "thinking of the answer is another precious second you could have been spending making your way to " +
                "the exit! If you get the answer right, the enemy may have the key to the exit and they will give " +
                "you the key, which will be added to your inventory. You cannot exit if you do not have the key! If " +
                "you try to exit without the key, you will be told to go back and find the key. Not all enemies hold " +
                "the key to the exit. If an enemy doesn't hold the key, they will give you some extra time because " +
                "you will need to find the enemy that has the key so that you can exit the maze and proceed to the " +
                "next level! If you answer the riddle incorrectly, you will just have wasted time, and you won't " +
                "know whether that was the enemy with the key (in fact, you might not be able to escape the level if " +
                "that enemy had the key because once you've encountered one enemy, you cannot re-encounter them, so " +
                "you may have to cop losing a life for that level...)\n" +

                "You have three lives for the whole game! You lose a life by running out of time before the " +
                "completion of the level. When you lose a life, you have to restart the level. If you lose all your " +
                "lives, you have to restart the game from the beginning (from level 0, that is).\n" +

                "Good luck! See if you can get to level 3 and escape! The mazes are different each time you run the " +
                "game, so run the game again if you're bored!");
        tutorialText.setWrappingWidth(1000);
        root.setAlignment(Pos.BOTTOM_LEFT);

        tutorialText.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        tutorialText.setFill(Color.WHITE);
        tutorialText.setTranslateX(0);
        tutorialText.setTranslateY(-300);

        VBox buttonsVBox = new VBox(10);
        buttonsVBox.setAlignment(Pos.BASELINE_RIGHT);
        Button back = createButton("GO BACK");

        buttonsVBox.getChildren().addAll(back);

        //set tutorial action
        back.setOnAction(event -> {
            GameMenu gameMenu = new GameMenu();
            try {
                gameMenu.start(new Stage());
                ((Stage) back.getScene().getWindow()).close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        root.getChildren().addAll(buttonsVBox, tutorialText);

        primaryStage.setScene(scene);
        primaryStage.setTitle("Tutorial");
        primaryStage.show();
    }

    private ImageView createMan() {
        ImageView man = new ImageView(mans[currentImageIndex]);
        man.setTranslateX(xPosition);
        man.setTranslateY(yPosition);

        transitionX = new TranslateTransition(Duration.millis(100), man);
        transitionY = new TranslateTransition(Duration.millis(100), man);

        return man;
    }

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * @author Peilin Liu
     * @param text text for the button to display
     * @return a button with the text on it
     */
    private Button createButton(String text) {
        Button button = new Button(text);
        button.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        button.setStyle("-fx-background-color: #333333; -fx-text-fill: #ffffff;");
        return button;
    }
}
