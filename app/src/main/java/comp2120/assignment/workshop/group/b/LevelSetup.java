package comp2120.assignment.workshop.group.b;

/**
 *
 * Static final variables that contain the setup for the levels (each time the class is called, a new maze for that
 * level is generated)
 *
 * @author Anousheh Moonen
 */
public class LevelSetup {

    static final Level LEVEL0 = new Level(0, 90, new SquareMaze(5, 1));
    static final Level LEVEL1 = new Level(1, 100, new SquareMaze(10, 3));
    static final Level LEVEL2 = new Level(2, 110, new SquareMaze(20,5));
    static final Level LEVEL3 = new Level(3, 150, new SquareMaze(30, 8));

    static final Level[] levels = {
            LEVEL0, LEVEL1, LEVEL2, LEVEL3
    };
}
