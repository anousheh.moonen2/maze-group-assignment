package comp2120.assignment.workshop.group.b;

/**
 * @author Jack Hodges
 * @author Anousheh Moonen
 * @author Peilin Liu
 */

import javafx.animation.*;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.layout.HBox;
import javafx.scene.control.Label;
import java.util.ArrayList;
import java.util.List;

public class LevelLayout extends Application{
    static Player player1;
    static Label timerLabel = new Label();

    Image[] mans = {
            new Image("/Assets/RunningMan1.png"),
            new Image("/Assets/RunningMan2.png")
    };

    // Determine which image of the player is currently displaying (we flip between images to give the illusion of
    // running)
    private int currentImageIndex = 0;
    private double wallThickness = 5;

    // The width and height of the maze in the frontend
    private int dimension = 600;

    // The frontend coordinates of the top left corner of the maze
    private Point startPoint = new Point(300, 30);

    // The player and enemy size (width = height for both the player and the enemy)
    private double characterSize;

    // How much the player moves by with each allowed key press
    double moveIncrement;

    // The number of moves it takes for a player to go from one cell to the next (affects the smoothness of the movement
    // - a higher ratio will result in smoother movement, but note that with a fixed ratio for all levels, the move
    // increment will be greater for the lower levels)
    private int ratio = 3;

    static StackPane answerPanel = new StackPane();

    // A list of the top left position of each cell (frontend position)
    private List<FrontEndPoint> mazePoints;

    /**
     * @param player1 an instance of Player that repreesnts the character
     */
    public LevelLayout(Player player1) {
        player1.resetPosition();
        this.player1 = player1;
    }

    BorderPane root = new BorderPane();

    // Default empty constructor which must be here if you want to start from the LevelLayout class
    public LevelLayout() {
        player1 = new Player(LevelSetup.LEVEL0);
    }

    /**
     * @author Jack Hodges
     * @param stage the primary stage for this application, onto which
     * the application scene can be set.
     * @throws Exception if there is an error setting up the stage
     */
    @Override
    public void start(Stage stage) throws Exception {
        setMazePointsArrays(player1.level.maze.grid);
        if (player1.level == LevelSetup.LEVEL2 || player1.level == LevelSetup.LEVEL3) {
            wallThickness = 2;
        }
        moveIncrement = (wallThickness + characterSize) / ratio;

        Image heartInput1;
        Image heartInput2;
        Image heartInput3;
        stage.setTitle("Maze");
        ImageView player = new ImageView(mans[currentImageIndex]);


        answerPanel.setAlignment(Pos.CENTER);

        var scene = new Scene(root, 1000, 800);
        stage.setScene(scene);
        // Set the background color of the root to black
        BackgroundFill backgroundFill = new BackgroundFill(Color.BLACK, CornerRadii.EMPTY, Insets.EMPTY);
        Background background = new Background(backgroundFill);
        root.setBackground(background);

        // HBox for the top of the UI
        HBox topBox = new HBox(2);
        topBox.setSpacing(20);

        // HEARTS CODE
        HBox heartBox = new HBox();
        heartBox.setSpacing(5);

        Image heartFull = new Image("/Assets/HeartFull.png");
        Image heartEmpty = new Image("/Assets/HeartEmpty.png");
        double heartWidth = 50;
        double heartHeight = 50;

        // Switch statement that checks the value of the player's lives and updates the UI
        switch (player1.lives) {
            case 0 -> {
                heartInput1 = heartEmpty;
                heartInput2 = heartEmpty;
                heartInput3 = heartEmpty;
            }
            case 1 -> {
                heartInput1 = heartFull;
                heartInput2 = heartEmpty;
                heartInput3 = heartEmpty;
            }
            case 2 -> {
                heartInput1 = heartFull;
                heartInput2 = heartFull;
                heartInput3 = heartEmpty;
            }
            default -> {
                heartInput1 = heartFull;
                heartInput2 = heartFull;
                heartInput3 = heartFull;
            }
        }

        ImageView heart1 = new ImageView(heartInput1);
        ImageView heart2 = new ImageView(heartInput2);
        ImageView heart3 = new ImageView(heartInput3);
        heart1.setFitWidth(heartWidth);
        heart1.setFitHeight(heartHeight);
        heart2.setFitWidth(heartWidth);
        heart2.setFitHeight(heartHeight);
        heart3.setFitWidth(heartWidth);
        heart3.setFitHeight(heartHeight);
        heartBox.getChildren().addAll(heart1, heart2, heart3);
        // END HEART BOX

        // START TIME
        HBox timeBox = new HBox();

        timerLabel = new Label(String.valueOf(player1.timeLeft));
        timerLabel.setFont(Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 60));
        timerLabel.setTextFill(Color.WHITE);
        timerLabel.setAlignment(Pos.CENTER);
        timerLabel.setTextAlignment(TextAlignment.CENTER);
        timeBox.setPadding(new Insets(0, 0, 0, 340));

        startCountdown();
        timeBox.getChildren().addAll(timerLabel);
        topBox.getChildren().addAll(heartBox, timeBox);
        // END OF TIME

        // END TOP BOX UI
        topBox.setPadding(new Insets(0, 0, 0, 10));
        root.setTop(topBox);


        // START BOTTOM BOX UI
        HBox bottomBox = new HBox();

        // START LEVEL AREA
        // String for level image
        String levelName;
        // change level image based on player level
        if (player1.level.levelNumber == 0) {
            levelName = "Level0";
        } else if (player1.level.levelNumber == 1) {
            levelName = "Level1";
        } else if (player1.level.levelNumber == 2) {
            levelName = "Level2";
        } else if (player1.level.levelNumber == 3) {
            levelName = "Level3";
        } else {
            levelName = "Level0";
        }
        Image levelImage = new Image("/Assets/" + levelName + ".png");
        ImageView levelShow = new ImageView(levelImage);
        levelShow.setFitHeight(50);
        levelShow.setFitWidth(50);
        // END LEVEL AREA

        // START KEY CODE
        HBox keyHBox = new HBox();
        keyHBox.setPadding(new Insets(0, 0, 0, 480));
        Image keyImage = new Image("/Assets/Key.png");
        ImageView keyView = new ImageView(keyImage);
        double keyWidth = 80;
        double keyHeight = 80;
        keyView.setFitWidth(keyWidth);
        keyView.setFitHeight(keyHeight);

        if (player1.inventory.contains(Item.KEY)) {
            keyView.setOpacity(1);
        } else {
            keyView.setOpacity(0.3);
        }

        keyHBox.getChildren().add(keyView);
        // END KEY CODE

        bottomBox.getChildren().addAll(levelShow, keyHBox);
        // add padding so image isn't in bottom left corner
        bottomBox.setPadding(new Insets(0, 0, -10, 20));
        root.setBottom(bottomBox);
        // END BOTTOM BOX

        // START CENTRE BOX
        Pane playerAndMaze = new Pane();

        // Setting the position to be the frontEnd version of the position of the starting player1 instance
        player.setX(startPoint.x + (player1.position.x) * (wallThickness + characterSize) + wallThickness);
        player.setY(startPoint.y + player1.position.y * (wallThickness + characterSize) + wallThickness);

        // This event checks for key presses and moves in directions only if the move is legal
        scene.setOnKeyPressed(event -> {
            try {
                KeyCode keyCode = event.getCode();
                player1.handleKeyEvent(keyCode, ratio);
                if (player1.inventory.contains(Item.KEY)) {
                    keyView.setOpacity(1);
                } else {
                    keyView.setOpacity(0.3);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (player1.readyToExit) {
                exit();
                playerAndMaze.getChildren().add(exit());
                player1.readyToExit = false;
            }

                currentImageIndex = (currentImageIndex + 1) % mans.length;
                player.setImage(mans[currentImageIndex]);
                double x = startPoint.x + wallThickness + player1.position.x * (wallThickness + characterSize)
                        + player1.movementInCellX * moveIncrement;
                double y = startPoint.y + wallThickness + (player1.position.y * (wallThickness + characterSize)
                        + player1.movementInCellY * moveIncrement);
                player.setX(x);
                player.setY(y);
        });
        player.setFitWidth(characterSize);
        player.setFitHeight(characterSize);
        playerAndMaze.getChildren().add(player);


        // This for loop iterates over the mazePoints arraylist and draws the maze. After drawing rectangles for all the
        // walls, it checks whether the cell has both an east and a south wall, in which case it draws an extra square
        // in the bottom-right corner because the walls wouldn't have extended that far. It also adds a door at the exit.
        for (int i = 0; i < mazePoints.size(); i++) {
            double wallLength = wallThickness + characterSize;
            double startX = startPoint.x + (wallThickness + characterSize) * player1.level.maze.cells.get(i).position.x;
            double startY = startPoint.y + (wallThickness + characterSize) * player1.level.maze.cells.get(i).position.y;
            if (player1.level.maze.cells.get(i).westWall) {
                Rectangle rectangle = createRectangle(startX, startY, wallThickness, wallLength);
                playerAndMaze.getChildren().add(rectangle);
            }
            if (player1.level.maze.cells.get(i).northWall) {
                Rectangle rectangle = createRectangle(startX, startY, wallLength, wallThickness);
                playerAndMaze.getChildren().add(rectangle);
            }
            if (player1.level.maze.cells.get(i).eastWall) {
                Rectangle rectangle = createRectangle(startX + wallLength, startY, wallThickness, wallLength);
                playerAndMaze.getChildren().add(rectangle);
            }
            if (player1.level.maze.cells.get(i).southWall) {
                Rectangle rectangle = createRectangle(startX, startY + wallLength, wallLength, wallThickness);
                playerAndMaze.getChildren().add(rectangle);
            }
            if (player1.level.maze.cells.get(i).eastWall && player1.level.maze.cells.get(i).southWall) {
                Rectangle rectangle = createRectangle(startX + wallLength, startY + wallLength,
                        wallThickness, wallThickness);
                playerAndMaze.getChildren().add(rectangle);
            }
            if (player1.level.maze.cells.get(i).position.equals(player1.level.maze.exit)) {
                Rectangle rectangle = new Rectangle(wallThickness, characterSize);
                rectangle.setX(startX + wallLength);
                rectangle.setY(startY + wallThickness);
                rectangle.setFill(Color.SADDLEBROWN);
                playerAndMaze.getChildren().add(rectangle);
                Rectangle fillIn1 = createRectangle(startX + wallLength, startY, wallThickness, wallThickness);
                Rectangle fillIn2 = createRectangle(startX + wallLength, startY + wallLength, wallThickness,
                        wallThickness);
                playerAndMaze.getChildren().add(fillIn1);
                playerAndMaze.getChildren().add(fillIn2);
            }
        }

        // Add enemies to maze
        for (Point enemy : player1.level.maze.enemyPoints) {
            double x = startPoint.x + wallThickness + enemy.x * (wallThickness + characterSize);
            double y = startPoint.y + wallThickness + enemy.y * (wallThickness + characterSize);
            ImageView enemyImage = createEnemy(x, y);
            playerAndMaze.getChildren().add(enemyImage);
        }

        /**
         * @Author Peilin Liu
         *
         *This is the gameplay menu, to let the player pause the game, and choose to restart or exit
         */
        //The pause menu
        Button pauseButton = createButton("Pause");

        pauseButton.setOnAction(event -> {
            timeline.stop();
            Rectangle overlayRect = new Rectangle(400, 300, Color.rgb(111, 25, 25, 0.73));

            Button continueButton = createButton("continue");
            Button restartButton = createButton("restart");
            Button exitButton = createButton(" exit");

            VBox buttons = new VBox(4,overlayRect,continueButton,restartButton,exitButton);

            buttons.setAlignment(Pos.BASELINE_RIGHT);

            StackPane popupPane = new StackPane(overlayRect, buttons);
            popupPane.setAlignment(Pos.CENTER);

            playerAndMaze.getChildren().add(popupPane);

            buttons.setAlignment(Pos.CENTER);
            continueButton.setOnAction(e -> {
                timeline.play();
                playerAndMaze.getChildren().removeAll(popupPane);
                player.requestFocus();
            });

            restartButton.setOnAction(e -> {
                Stage stage2 = (Stage) exitButton.getScene().getWindow();
                stage2.close();
                LevelLayout tutorial = new LevelLayout();
                try {
                    tutorial.start(new Stage());
                    ((Stage) restartButton.getScene().getWindow()).close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            });

            exitButton.setOnAction(e -> {
                Stage stage2 = (Stage) exitButton.getScene().getWindow();
                stage2.close();
            });

        });

        playerAndMaze.getChildren().add(pauseButton);


        root.setCenter(playerAndMaze);

        playerAndMaze.getChildren().add(answerPanel);

        // END CENTRE BOX

        stage.show();
    }


    // This method links to the next level menu and enables the player to exit if they are at the exit.
    // If they are on the highest level, they will be returned to the game menu.
    public StackPane exit() {
        int initialLevelNumber = player1.level.levelNumber;
        if (player1.escapeLevel(player1.level)) {
            if (initialLevelNumber == 3) {
                GameMenu gameMenu = new GameMenu();
                gameMenu.start(new Stage());
            }
            Stage stage = (Stage) timerLabel.getScene().getWindow();
            stage.close();
            timeline.stop();
            JumpPage jumpPage = new JumpPage(player1);
            try {
                jumpPage.start(new Stage());
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        /**
         * @author Peilin Liu
         *
         */
        else {
            StackPane warning = new StackPane();
            Rectangle waringRect = new Rectangle(800, 100, Color.rgb(111, 25, 25, 0.73));
            Label label = new Label("where do you keep your key? In your dream?");
            label.setFont(Font.font("Arial", FontWeight.BOLD, 30));
            label.setTextFill(Color.WHITE);
            VBox popupContent = new VBox(label);
            popupContent.setAlignment(Pos.CENTER);
            StackPane.setAlignment(popupContent, Pos.CENTER);
            StackPane popupPane = new StackPane(waringRect, popupContent);
            popupPane.setAlignment(Pos.CENTER);

            warning.getChildren().add(popupPane);

            Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(3), e -> warning.setVisible(false)));
            timeline.play();
            return warning;
        }

        return new StackPane();
    }

    /**
     * @author Peilin Liu
     *
     * This is the method that makes player interacts with enemies
     */
    public static void encounterEnemy(Enemy enemy) throws Exception {
        //handle interaction and check answer
        RiddlePanel riddlePanel = new RiddlePanel(new Riddle(enemy.riddle.question, "yes", enemy.riddle.answerNum), answerPanel, enemy, player1);
        riddlePanel.startRiddle(new Riddle(enemy.riddle.question, "yes", enemy.riddle.answerNum), answerPanel);
    }

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * @author Jack Hodges
     * @param x double representing the x location of the rectangle
     * @param y double representing the y location of the rectangle
     * @param width double representing the width of the rectangle
     * @param height double representing the height of the rectangle
     * @return a rectangle with the above properties
     */
    public static Rectangle createRectangle(double x, double y, double width, double height) {
        Rectangle newRec = new Rectangle(width, height);
        newRec.setX(x);
        newRec.setY(y);
        newRec.setFill(Color.WHITE);
        return newRec;
    }

    /**
     * @author Jack Hodges
     * @param x double representing x location of enemy
     * @param y double representing y location of enemy
     * @return an instance of the enemy at specified location
     */
    public ImageView createEnemy(double x, double y) {
        Image newImage = new Image("/Assets/Enemy.png");
        ImageView newImageView = new ImageView(newImage);
        newImageView.setFitHeight(characterSize);
        newImageView.setFitWidth(characterSize);
        newImageView.setX(x);
        newImageView.setY(y);
        return newImageView;
    }

    /**
     * @author Anousheh Moonen
     * @param cellsToConvert an array of Cell[][] that represents back end points that need to be converted to front end points
     * @return a List<FrontEndPoint> that represent real locations on the screen
     */
    private List<FrontEndPoint> backendToFrontEndCoords(Cell[][] cellsToConvert) {
        characterSize = (dimension - wallThickness*(cellsToConvert.length + 1))/cellsToConvert.length;
        ArrayList array = new ArrayList<>();
        for (Cell cell: player1.level.maze.cells) {
            FrontEndPoint pointToAdd = new FrontEndPoint(startPoint.x + (cell.position.x * (wallThickness + characterSize)),
                    startPoint.y + (cell.position.y * (wallThickness + characterSize)));
            array.add(pointToAdd);
        }
        return array;
    }

    /**
     * @author Anousheh Moonen
     * @param cells takes in the Cell[][] and passes it to backendToFrontEndCoords, then sets the points of the maze to the return
     */
    private void setMazePointsArrays(Cell[][] cells) {
        this.mazePoints = backendToFrontEndCoords(cells);
    }

    static Timeline timeline = new Timeline(0);
    private void startCountdown() {
        timeline = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                player1.timeLeft--;
                if (player1.timeLeft > 10) {
                    timerLabel.setTextFill(Color.WHITE);
                    timerLabel.setText(String.valueOf(player1.timeLeft));
                }
                else if (player1.timeLeft == 10) {
                    timerLabel.setTextFill(Color.DARKORANGE);
                    timerLabel.setText(String.valueOf(player1.timeLeft));
                }
                else {
                    timerLabel.setTextFill(Color.DARKORANGE);
                    timerLabel.setText("0" + player1.timeLeft);
                }

                if (player1.timeLeft == 0) {
                    player1.resetTime();
                    // Countdown finished, stop the timer
                    timerLabel.setTextFill(Color.DARKRED);
                    timeline.stop();
                    /**
                     * @author Peilin Liu
                     */
                    Stage stage = (Stage) timerLabel.getScene().getWindow();
                    stage.close();
                    if(player1.lives > 1) {
                        player1.lives -= 1;
                        timeline.stop();
                        FailAnswerPage failAnswerPage = new FailAnswerPage(player1);
                        try {
                            failAnswerPage.start(new Stage());
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    } else if(player1.lives == 1) {
                        player1.resetLives();
                        timeline.stop();
                        FailPage failPage = new FailPage(player1);
                        try {
                            failPage.start(new Stage());
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                }
            }
        }));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }


    private Button createButton(String text) {
        Button button = new Button(text);
        button.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        button.setStyle("-fx-background-color: #333333; -fx-text-fill: #ffffff;");
        return button;
    }
}

