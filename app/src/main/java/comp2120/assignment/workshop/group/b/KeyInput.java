package comp2120.assignment.workshop.group.b;

/**
 * @author Jack Hodges
 * @author Anousheh Moonen
 * @author Peilin Liu
 */

import javafx.scene.input.KeyCode;

public enum KeyInput {
    UP(KeyCode.UP),
    DOWN(KeyCode.DOWN),
    LEFT(KeyCode.LEFT),
    RIGHT(KeyCode.RIGHT),
    W(KeyCode.W),
    A(KeyCode.A),
    S(KeyCode.S),
    D(KeyCode.D);

    private final KeyCode keyCode;

    KeyInput(KeyCode keyCode) {
        this.keyCode = keyCode;
    }

    public KeyCode getKeyCode() {
        return keyCode;
    }
}