/**
 * @author Anousheh Moonen
 */
package comp2120.assignment.workshop.group.b;

/**
 * @author Anousheh Moonen
 */
public class Enemy {
    // Each enemy has a riddle
    Riddle riddle;

    // 1 if the enemy has the key, 0 if not
    int key;

    // The extra time that the enemy has available to give to the player (0 if they have the key)
    float time;

    // Position of the enemy
    Point position;

    /**
     * @author Anousheh Moonen
     * @param riddle an instance of Riddle that represents the riddle the enemy will say
     * @param key an int (1 or 0) that represents if the enemy has the key
     * @param time a float for how much extra time the player gets for correctly answering the riddle
     * @param position a point for the position of the enemy in the maze
     */
    public Enemy(Riddle riddle, int key, float time, Point position) {
        this.riddle = riddle;
        this.key = key;
        this.time = time;
        this.position = position;
    }

    // Empty constructor to enable easy setting of the fields after creation in the level classes
    public Enemy() {

    }
}
