package comp2120.assignment.workshop.group.b;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * @author Peilin Liu
 */
public class Tutorial extends Application {

    private double xPosition = 0; // defult position of player
    private double yPosition = 300; // defult position
    private double prevXPosition = 0; // defult position of player
    private double prevYPosition = 300; // defult position

    Image[] mans = {
            new Image("/Assets/RunningMan1.png"),
            new Image("/Assets/RunningMan2.png")
    };

    private int currentImageIndex = 0;
    private TranslateTransition transitionX;
    private TranslateTransition transitionY;
    @Override
    public void start(Stage primaryStage) {
        Canvas canvas = new Canvas(1200, 1000);
        StackPane root = new StackPane();
        // set scene
        Scene scene = new Scene(root, 1200, 1000);
        root.setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));
//        root.setAlignment(Pos.CENTER);
        ImageView newMan = createMan();
        root.getChildren().add(newMan); // keep the character
        newMan.requestFocus();
        // set instruction text
        Text tutorialText = new Text("Welcome to tutorial, now press wasd to move");

        //create enemy
        Rectangle enemy = createEnemy();
        enemy.setTranslateX(0);
        enemy.setTranslateY(0);
        root.getChildren().add(enemy);

        newMan.setOnKeyPressed(event1 -> {
            KeyCode code = event1.getCode();
            if (code == KeyCode.W) {
                yPosition -= 10;
            } else if (code == KeyCode.S) {
                yPosition += 10;
            } else if (code == KeyCode.A) {
                xPosition -= 10;
            } else if (code == KeyCode.D) {
                xPosition += 10;
            }
            currentImageIndex = (currentImageIndex + 1) % mans.length;
            newMan.setImage(mans[currentImageIndex]);
            newMan.setTranslateX(xPosition);
            newMan.setTranslateY(yPosition);
            if (code == KeyCode.W||code == KeyCode.S||code == KeyCode.A||code == KeyCode.D) {
                tutorialText.setText("Well done! Now try to get close to the enemey and press E to interact");
            }
            if (newMan.getBoundsInParent().intersects(enemy.getBoundsInParent())) {
                xPosition = prevXPosition;
                yPosition = prevYPosition;
            }

            prevXPosition = xPosition;
            prevYPosition = yPosition;
            newMan.setTranslateX(xPosition);
            newMan.setTranslateY(yPosition);
            double distance = Math.sqrt(Math.pow((xPosition - enemy.getTranslateX()), 2) + Math.pow((yPosition - enemy.getTranslateY()), 2));

            System.out.println(distance);
            if (distance < 200) {
                if (event1.getCode() == KeyCode.E) {
//                    RiddlePanel riddlePanel = new RiddlePanel (new Riddle("Is Peilin handsome?", "yes",1), root, );
                    try {
//                        riddlePanel.startRiddle(new Riddle("Is Peilin handsome?", "yes",1), root);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        });

        Button pauseButton = createButton("Pause");
        pauseButton.setTranslateX(450);
        pauseButton.setTranslateY(-450);

        pauseButton.setOnAction(event -> {
            Rectangle overlayRect = new Rectangle(400, 300, Color.rgb(111, 25, 25, 0.73));

            Button continueButton = createButton("continue");
            Button restartButton = createButton("restart");
            Button exitButton = createButton(" exit");

            VBox buttons = new VBox(4,overlayRect,continueButton,restartButton,exitButton);

            StackPane popupPane = new StackPane(overlayRect, buttons);
            popupPane.setAlignment(Pos.CENTER);

            root.getChildren().add(popupPane);

            buttons.setAlignment(Pos.CENTER);
            continueButton.setOnAction(e -> {
                root.getChildren().removeAll(popupPane);
                newMan.requestFocus();
            });

            restartButton.setOnAction(e -> {
                Stage stage = (Stage) exitButton.getScene().getWindow();
                stage.close();
                Tutorial tutorial = new Tutorial();
                try {
                    tutorial.start(new Stage());
                    ((Stage) restartButton.getScene().getWindow()).close();
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            });

            exitButton.setOnAction(e -> {
                Stage stage = (Stage) exitButton.getScene().getWindow();
                stage.close();
            });

        });

        root.getChildren().add(pauseButton);

        tutorialText.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        tutorialText.setFill(Color.WHITE);
        tutorialText.setTranslateX(0);
        tutorialText.setTranslateY(-300);
        root.getChildren().add(tutorialText);

        //show
        primaryStage.setScene(scene);
        primaryStage.setTitle("Tutorial");
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

    private ImageView createMan() {
        ImageView man = new ImageView(mans[currentImageIndex]);
        man.setTranslateX(xPosition);
        man.setTranslateY(yPosition);

        transitionX = new TranslateTransition(Duration.millis(100), man);
        transitionY = new TranslateTransition(Duration.millis(100), man);

        return man;
    }

    /**
     * @author Peilin Liu
     * @param text text for the button to display
     * @return a button with the text on it
     */
    private Button createButton(String text) {
        Button button = new Button(text);
        button.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        button.setStyle("-fx-background-color: #333333; -fx-text-fill: #ffffff;");
        return button;
    }

    private Rectangle createEnemy(){
        return new Rectangle(50, 50, Color.RED);
    }
}
