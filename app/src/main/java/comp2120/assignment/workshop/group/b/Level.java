package comp2120.assignment.workshop.group.b;
/**
 * @author Anousheh Moonen
 */

public class Level {
    public int levelNumber;
    public SquareMaze maze;
    public int timeToComplete;

    /**
     * @author Anousheh Moonen
     *
     * @param levelNumber int representing the number of the level (0, 1, 2 or 3)
     * @param timeToComplete int for how long the player has to complete this level
     * @param maze instance of SquareMaze that represents the maze points and location
     */
    public Level(int levelNumber, int timeToComplete, SquareMaze maze) {
        this.levelNumber = levelNumber;
        this.timeToComplete = timeToComplete;
        this.maze = maze;
    }
}

