package comp2120.assignment.workshop.group.b;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * @author Peilin Liu
 */
public class SuccessAnswerPage extends Application {
    @Override
    public void start(Stage primaryStage) {
        // create Canvas(HD)
        Canvas canvas = new Canvas(1200, 1000);

        Text tutorialText = new Text("You got the riddle right!");

        Pane root = new Pane();
        Scene scene = new Scene(root, 200, 200);
        primaryStage.setScene(scene);

        primaryStage.setTitle("SuccessAnswerPage");

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
